user 'knife' do
  comment 'A user for knife commands'
  supports :manage_home => true
  home '/home/knife'
  shell '/bin/bash'
end

directory '/home/knife' do
  action :create
  mode 0750
  owner 'knife'
  group 'jenkins'
end

directory '/home/knife/.ssh' do
  action :create
  mode 0750
  owner 'knife'
  group 'knife'
end

file '/home/knife/.ssh/id_rsa' do
  content data_bag_item('keys', 'knife')['knife_private']
  mode 0600
  owner 'knife'
  group 'knife'
end

file '/opt/jenkins/.ssh/knife_ssh' do
  content data_bag_item('keys', 'knife')['knife_private']
  mode 0600
  owner 'jenkins'
  group 'jenkins'
end

gkey = data_bag_item('keys', 'knife')['knife_public']

file '/home/knife/.ssh/authorized_keys' do
  content gkey
  user 'knife'
  group 'knife'
  mode '0640'
end

sudo 'jenkins' do
  user      "jenkins"  
  runas     'knife'
  nopasswd  true
  commands  ['ALL']
  defaults  ['!requiretty']
end

sudo 'knife' do
  user      "knife"
  runas     'root'
  nopasswd  true
  commands  ['ALL']
  defaults  ['!requiretty']
end

group "jenkins" do
  action :modify
  members 'knife'
  append true
end

group "knife" do
  action :modify
  members 'jenkins'
  append true
end
