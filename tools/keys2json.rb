#!/opt/chefdk/embedded/bin/ruby

require 'json'

keys_dir = "keys"
data_bags_dir = "data_bags/keys"

required_keys = ["aws", "git", "knife"]

required_keys.each do |k|
  if File.file? "#{keys_dir}/#{k}"
    puts "Found key #{k}\n"
    data_bag = "#{data_bags_dir}/#{k}.json"
    bag_content = JSON.parse(File.read(data_bag))
    privkey = File.read("#{keys_dir}/#{k}")
    pubkey = ""
    if File.file? "#{keys_dir}/#{k}.pub"
      pubkey = File.read("#{keys_dir}/#{k}.pub")
      bag_content["#{k}_public"] = pubkey
    end
    bag_content["#{k}_private"] = privkey
    File.open(data_bag, 'w') do |f|
      f.puts JSON.pretty_generate(bag_content)
      puts "Wrote new content to ./#{data_bag}"
    end
  else
    puts "Error, key #{k} not found in ./#{keys_dir}"
    exit 1
  end
end
