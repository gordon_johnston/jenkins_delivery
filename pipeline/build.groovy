import groovy.time.TimeCategory
import hudson.AbortException
import hudson.console.HyperlinkNote
import java.util.concurrent.CancellationException
import groovy.json.JsonSlurper

def start        = new Date()
def versionType  = "${VERSION_TYPE}"
def rubyPath     = '/opt/chefdk/embedded/bin/ruby --external-encoding=UTF-8 --internal-encoding=UTF-8 '
def repo         = "${COOKBOOK_REPO}"
nodeLabel        = "master"

def findCookbookName = '''#!/opt/chefdk/embedded/bin/ruby
require 'chef/cookbook/metadata'
metadata_file = 'metadata.rb'
metadata = Chef::Cookbook::Metadata.new
metadata.from_file(metadata_file)
`echo "#{metadata.name}" > cookbook.properties`
'''

def findVersion = '''#!/bin/bash

LATEST=`git for-each-ref refs/tags --sort=-taggerdate --format='%(refname:short)' --count=1`

if [ "$LATEST" = "" ]
 then
 LATEST="v0.0.0"
fi

VERSION=${LATEST/v}
MAJOR=$(echo $VERSION | awk -F'.' '{print $1}')
MINOR=$(echo $VERSION | awk -F'.' '{print $2}')
PATCH=$(echo $VERSION | awk -F'.' '{print $3}')

case "''' + "${VERSION_TYPE}" + '''" in
 "major") MAJOR=$(( MAJOR + 1 ))
          MINOR=0
          PATCH=0
 ;;
 "minor") MINOR=$(( MINOR + 1 ))
          PATCH=0
 ;;
 "patch") PATCH=$(( PATCH + 1 ))
 ;;
esac

NEW_VERSION="${MAJOR}.${MINOR}.${PATCH}"

printf "Latest tag from remote is $LATEST\n"
printf "New version will be v$NEW_VERSION\n"

echo "${NEW_VERSION}" > env.properties
'''

node(nodeLabel) {
    stage name: "Prepare Environment", concurrency: 1
    def branch = "master"

    checkout([$class: 'GitSCM', branches: [[name: branch]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'WipeWorkspace']], submoduleCfg: [], userRemoteConfigs: [[url: repo]]])

    sh findCookbookName
    def cookbookName = readFile('cookbook.properties').trim()
    def pwd      = pwd()
    def pwdClean = pwd.replaceAll(" ", "\\\\ ")

    currentBuild.setDisplayName("${cookbookName} #${currentBuild.number}")

    sh findVersion
    def vers = readFile('env.properties').trim()
    
    def bumpMetadata = '''#!/bin/bash
    export METADATA="''' + "${vers}" + '''"
    sed -i "s/^\\(version.*\\)'.*'$/\\1'${METADATA}'/" metadata.rb\n'''
    
    sh bumpMetadata

    def tagRepo = '''#!/bin/bash
    export VERS="v''' + "${vers}" + '''"
    printf "Committing metadata.rb..."
    git config --global user.name "Jenkins Delivery"
    git config --global user.email "jenkins.delivery@nowhere.local"
    git commit -am "Version ${VERS}"
    printf "Creating tag..."
    git tag -a ${VERS} -m "Automatically tagged by Jenkins Delivery ''' + "${env.NODE_NAME}" + '''"
    git push --tags
    git push origin HEAD:''' + "${branch}" + '''\n'''

    currentBuild.setDisplayName("${cookbookName} - ${vers}")

    stage name: "Push Tag", concurrency: 1
    wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm', 'defaultFg': 1, 'defaultBg': 2]) {
      sh tagRepo
   }
    stage name: "Upload to Supermarket", concurrency: 1
    wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm', 'defaultFg': 1, 'defaultBg': 2]) {
      sh "knife supermarket share --supermarket-site https://chef-supermarket.test.gharris.uk -c /opt/jenkins/orgs/chef-server.test.gharris.uk/management-knife.rb ${cookbookName} -o .."
   }

   stage name: "Deploy to Chef Server", concurrency: 1

   build job: 'UploadChefCode', parameters: [[$class: 'StringParameterValue', name: 'CHEF_ENV', value: 'skybet_public'], [$class: 'StringParameterValue', name: 'BUILD_TAG', value: 'master']]
}
