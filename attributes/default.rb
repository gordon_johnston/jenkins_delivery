default['sbg_jenkins_delivery']['git_url'] = "https://www.kernel.org/pub/software/scm/git"
default['sbg_jenkins_delivery']['git_version'] = "2.7.2"
default['sbg_jenkins_delivery']['checksum'] = "58959e3ef3046403216a157dfc683c4d7f0dd83365463b8dd87063ded940a0df"

default['system']['domain_name'] = node['route53_domain']
default['resolver']['search'] = node['route53_domain']

default['chef-server']['api_fqdn'] = "chef-server-int.#{node['route53_domain']}"
default['chef-server']['accept_license'] = true
default['supermarket_omnibus']['chef_server_url'] = "https://chef-server.#{node['route53_domain']}"

default['chef-server']['addons'] = ["manage"]

default['sbg_jenkins_delivery']['aws_ssh_key_id'] = 'jenkins-delivery'
