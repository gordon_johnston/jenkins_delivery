default['jenkins']['master'].tap do |master|

  master['install_plugins'] = %W{workflow-aggregator ansicolor git build-monitor-plugin email-ext envinject git-client instant-messaging 
                                 junit matrix-project pam-auth script-security ssh-agent ssh-credentials 
                                 analysis-core translation warnings pwauth parameterized-trigger rebuild 
                                 build-name-setter notification build-token-root nodelabelparameter embeddable-build-status rich-text-publisher-plugin modernstatus pipeline-stage-view}

  master['version'] = "2.8-1.1"

  master['chef_servers'] = [ node['chef-server']['api_fqdn'] ]
  
  master['jvm_options'] = '-Xms256m -Xmx256m'

  master['jenkins_args'] = nil

  master['user'] = 'jenkins'

  master['api_user'] = 'admin'

  master['group'] = 'jenkins'

  master['listen_address'] = '0.0.0.0'

  master['port'] = 8080

  master['home'] = '/opt/jenkins'

  master['chef_orgs'] = "#{node['jenkins']['master']['home']}/orgs"

  master['validators'] = "/opt/validators"

  master['chef_users'] = "#{node['jenkins']['master']['home']}/users"
  
  master['log_directory'] = '/var/log/jenkins'

  master['repository'] = 'http://pkg.jenkins-ci.org/redhat'
   
  master['package_checksum'] = 'e82573637ae39da97c53a6d5d81cf7e82ca6ac739023352e7328fff51c7dd454'

end
