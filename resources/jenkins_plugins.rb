resource_name :jenkins_plugins

property :password, [String, nil], default: node['jenkins']['master']['api_pass']
property :plugins, Array

load_current_value do
  if ::File.exist? '/opt/jenkins/secrets/initialAdminPassword'
    password IO.read('/opt/jenkins/secrets/initialAdminPassword').chomp
  end
end

action :install do
  require 'jenkins_api_client'
  client = JenkinsApi::Client.new( :server_ip => '0.0.0.0',
                                   :username => node['jenkins']['master']['api_user'],
                                   :password => password)
  restart_required = false
  pm = JenkinsApi::Client::PluginManager.new(client)
  plugins.each do |p|
    unless pm.list_installed.keys.include? p
      puts "Installing plugin #{p}"
      pm.install(p)
      sleep 15
      restart_required = true
    end
  end
  
  if restart_required
    system = JenkinsApi::Client::System.new(client)
    system.quiet_down
    system.restart(false)
    system.wait_for_ready
    system.cancel_quiet_down
  end
end
