#!/bin/env ruby
# encoding: utf-8

require 'rubygems'
require 'colorize'

# Need to set this for running from groovy
ENV['LANG'] = 'en_GB.UTF-8'
# Real-time please!
STDOUT.sync = true

require '/opt/jenkins/chef/logger.rb'
logger = Log.new

# helper functions

def fail_run( reason=nil, logger )

    unless reason.nil?
        logger.error reason
    end
    logger.error "Data bag upload failed!"
    exit 1
end


# expects a numeric return from fn
def timed_function( fn, desc, args=nil, logger )

    logger.info "Starting #{desc}..."
    start  = Time.now

    ret = send( fn, *args, logger )

    finish = Time.now - start
    mins   = finish / 60
    finish = finish % 60

    fin = "Finished #{desc} in #{mins.to_i} mins #{finish.round( 2 )} sec"

    if ret == 0
        logger.notice "#{fin}."
    else
        fail_run( "#{fin} with #{ret} error#{ret == 1 ? '' : 's'}.", logger )
    end
end


def forked_command( list, max, fn, logger )

    items = list.clone
    count = 0
    codes = [ ]

    while items.count > 0
        while count < max and items.count > 0
            item = items.pop
            pid  = Process.fork

            if pid.nil?
                rc = send( fn, item )
                if rc != 0
                    logger.warn "Calling '#{fn}' failed (#{rc}) on: #{item}."
                end
                exit rc
            else
                count += 1
            end
        end

        pid,status = Process.waitpid2( 0, 0 )
        codes.push( status.exitstatus )
        count -= 1
    end

    Process.waitall.each do |ret|
        codes.push( ret[1].exitstatus )
    end

    codes.delete( 0 )
    codes.count
end

def databag_single( bag )

    files = Dir.entries( "data_bags/#{bag}" ).select { |e| e.match( /\.json$/ ) }

    ret = 0

    `knife data bag create #{bag} -c #{$KNIFE}`
    ret += $?.to_i >> 8

    `knife data bag from file #{bag} #{files * ' '} -c #{$KNIFE}`
    ret += $?.to_i >> 8

    ret
end


def databag_upload( max, logger )
    bags = Dir.entries( 'data_bags' ).select { |e| e.chr != '.' and File.directory?( "data_bags/#{e}" ) }.sort
    forked_command( bags, max, 'databag_single', logger )
end




# flow of control starts here

# get our args
unless ARGV.length >= 1
  puts 'Usage: data-bag-upload.rb <environment>'.colorize( :red )
  exit 1
end

require 'json'

$ENV   = ARGV[0]

chef_api=JSON.parse(File.read("/opt/jenkins/orgs/#{$ENV}-org.json"))['chef-api']

# knife config
$KNIFE = "/opt/jenkins/orgs/#{chef_api}/#{$ENV}-knife.rb"
unless File.file?( $KNIFE )
    fail_run( "Knife config #{$KNIFE} does not exist, please verify the slave has the correct knife config.", logger )
end
logger.info "Using knife config #{$KNIFE}"

# and fork max concurrency 
maxf = 3

timed_function( 'databag_upload', 'data bag upload', [ maxf ], logger )
