require 'net/ssh/multi'
require 'timeout'
require 'English'
require_relative 'commandrun'

module Sbg
  # Runs a command only many hosts. Attach event listeners to get notified when something interesting happens
  # on a host.
  #
  # Events
  #   | name    | parameters                         | description
  #   | ------- | ---------------------------------- | -------------------------------------------------------------------
  #   | connect | hostname                           | When a connection is made to a host (just before the command is ran)
  #   | idle    | list of remaining hosts            | When no events have been triggered in 30 seconds
  #   | stdout  | hostname, text                     | Command has outputted something to standard out
  #   | stderr  | hostname, text                     | Command has outputted something to standard error
  #   | exit    | hostname, Sbg::CommandRun instance | Command has exited on a node
  #   | error   | hostname, message                  | An error occured before or during running the command on a node
  #
  # Attach events by calling on_<event_name> method with a block. For example;
  #
  #   multissh = Sbg.MultiSSH.new
  #
  #   multissh.on_connect do |hostname|
  #     puts "Connected to #{hostname}!"
  #   end
  #
  #   mulitssh.on_exit do |hostname, run|
  #     puts "#{hostname} exited with #{run.exit_code}"
  #   end
  #
  #   mutlissh.execute(['foo.com', 'bar.com'], 'echo hello')
  class MultiSSH
    SUDO_PROMPT = 'multissh sudo password:'

    [:connect, :idle, :stdout, :stderr, :exit, :error].each do |event|
      # Create on_<event> and trigger_<event> methods
      define_method("on_#{event}") do |&block|
        @subscriptions[event] << block
      end

      define_method("trigger_#{event}") do |*args|
        @subscriptions[event].each { |fn| fn.call(*args) }
      end
    end

    def initialize(start_options)
      @subscriptions = Hash.new { |h, v| h[v] = [] }
      @start_options = {
        on_error: proc do |session|
          cleanup_connection(session.host, -1, $ERROR_INFO.message)
        end
      }.merge(start_options)
    end

    # Runs command on host_list. connection_options are passed through to Net::SSH::Session
    # This method blocks until all hosts have ran the command.
    def execute(host_list, command, connection_options)
      @seen_hosts      = Hash.new { |h, v| h[v] = Sbg::CommandRun.new(v, command) }
      @remaining_hosts = host_list.dup
      command          = add_sudo_prompt(command)

      Net::SSH::Multi.start(@start_options) do |session|
        set_hosts(session, host_list, connection_options)

        session.open_channel do |channel|
          attach_event_triggers(channel, connection_options[:password])
          request_pty(channel)
          send_exec(channel, command)
        end

        do_loop(session)
      end

      @seen_hosts
    end

    private

    def request_pty(channel)
      channel.request_pty do |ch, success|
        next if success
        trigger_error(ch[:host], 'could not obtain pty')
      end
    end

    def send_exec(channel, command)
      channel.exec(command) do |_, success|
        hostname = channel[:host]
        @seen_hosts[hostname].start

        if success
          trigger_connect(hostname)
          next
        else
          cleanup_connection(hostname, -2, "Unable to run '#{command}'. Unknown error")
        end
      end
    end

    def do_loop(session)
      last_loop = Time.now
      session.loop(30) do
        next session.busy? if Time.now - last_loop < 30
        last_loop = Time.now

        # 30 seconds have passed since the block was last called
        # This implies no Net::SSH::Multi events have been triggered in that time.
        # Lets trigger an idle event to let the calling code know we're still working.
        trigger_idle(@remaining_hosts)
        next session.busy?
      end
    end

    def set_hosts(session, host_list, connection_options)
      host_list.each do |host|
        host       = [host, 22] unless host.is_a?(Array)
        host, port = host

        connection_options[:port] ||= port

        session.use(host.strip, connection_options)
      end
    end

    def attach_event_triggers(channel, password)
      channel.on_request('exit-status') do |hchannel, data|
        hostname = hchannel[:host]
        @seen_hosts[hostname].exit(data.read_long)
      end

      channel.on_close do |hchannel|
        hostname = hchannel[:host]
        cleanup_connection(hostname)
      end

      channel.on_data do |hchannel, data|
        hostname = hchannel[:host]
        data.force_encoding('UTF-8').strip!
        @seen_hosts[hostname].append_stdout(data)
        trigger_stdout(hostname, data)

        close_if_password_expired(channel, data)
        sudo_password_send(channel, data, password)
      end

      channel.on_extended_data do |hchannel, _, data|
        hostname = hchannel[:host]
        @seen_hosts[hostname].append_stderr(data.strip)
        trigger_stderr(hostname, data.strip)
      end

      channel.on_open_failed do |hchannel, code, reason|
        hostname = hchannel[:host]
        cleanup_connection(hostname, -3, "#{code}: #{reason}")
      end
    end

    def add_sudo_prompt(command)
      fail 'Command started with sudo but specified a custom prompt. Remove -p option' if command =~ /^sudo -p/
      command.sub('sudo', %(sudo -p '#{SUDO_PROMPT}'))
    end

    def close_if_password_expired(channel, data)
      return unless data.downcase =~ /You must change your password now and login again!/i
      hostname = channel[:host]
      channel.close

      cleanup_connection(hostname, -1, 'User was asked to change password')
    end

    def cleanup_connection(hostname, exit_code = nil, error = nil)
      @seen_hosts[hostname].exit(exit_code) if exit_code

      @remaining_hosts.delete(hostname)

      if error
        @seen_hosts[hostname].error = error
        trigger_error(hostname, error)
      else
        trigger_exit(hostname, @seen_hosts[hostname])
      end
    end

    def sudo_password_send(channel, data, password)
      return unless (data =~ /^#{SUDO_PROMPT}/) == 0
      if password
        channel.send_data("#{password}\n")
      else
        channel.close
        error_msg = 'Was presented with a sudo prompt, but no password was given. Closing connection.'
        cleanup_connection(channel[:host], -4, error_msg)
      end
    end
  end
end
