#!/opt/chefdk/embedded/bin/ruby
require 'mixlib/cli'
require_relative 'sbg/multissh'
require_relative 'sbg/cli'

class MultiSSHCli
  include Mixlib::CLI
  include Sbg::CLI

  banner <<-BANNER
#{$PROGRAM_NAME} - Execute command via SSH on a list of servers

Reads a list of nodes from STDIN or environment variable (see: injecting nodes)

Injecting Nodes:
  There are two ways to pass a list of nodes to this program.

  1. NODE_LIST environment variable
    A newline seperated list of nodes hostnames.
    Example: $ NODE_LIST=$'foo.example.com\\nbar.example.com' ./knife/execute.rb --command 'echo hello'

  2. Pipe via STDIN
    As soon as the pipe is empty the commands are executed. Further writes to STDIN will be picked up when the current batch of nodes have completed.
    Example: $ echo -e "foo.example.com\\nbar.example.com" | ./knife/execute.rb --command 'echo hello'

Controlling output:
  This program lets to disable various types of output from being printed to STDOUT. This gives you flexibility to use this program in an attended or unattended context.

  Default arguments will print all output types as soon as they are available.

  Output Types:
    * connection: Prints when a connection to a remote host has been established.
        Example output: [CONNECTION] foobar.example.com
        Disable argument: --no-connection_output

    * command: Prints STDOUT and STDERR output from command.
        Example output: [STDOUT] foobar.examle.com: I've been printed using echo!
        Example output: [STDERR] foobar.examle.com: foobar: command not found
        Disable argument: --no-command_output

    * idle: Every 30 seconds of inactivity (for example duing long running commands) print the number of hostnames that have yet to finish executing command
        Example output: [MONITOR] Final 2 node(s) remaining: foo.example.com, bar.example.com
        Disable argument: --no-idle_output

    * exit: Prints exit code and time taken to run command when host finishes execution
        Example output: [EXITED] foobar.example.com: Exit code 0. Took 1 minute and 10 seconds
        Disable argument: '--[no-]exit_output'

    * error: Prints error message if a connection to the remote host could not be established
        Example output: [ERROR] foobarbaz.example.com: getaddrinfo: Temporary failure in name resolution
        Disable argument: '--[no-]error_output'

  Grouping Command Ouput:
    Running a command that prints lots of lines to STDOUT/STDERR on many hosts with a concurrency greater than one can result in a flood of disjointed output. The output
    is printed in realtime but at the expense of readability. Setting `--output grouped` will queue command output until the command has finished execution. The command
    output is printed all at once.

    The example below illustrates the different `--output` arguments.

    $ echo -e "foo.example.com\\nbar.example.com" | ./knife/execute.rb --command 'echo hello && sleep 5 && echo world'

    [CONNECTION] foo.example.com
    [CONNECTION] bar.example.com
    [STDOUT] foo.example.com: hello
    [STDOUT] bar.example.com: hello
    [STDOUT] foo.example.com: world
    [EXITED] foo.example.com: Exit code 0. Took 5 seconds
    [STDOUT] bar.example.com: world
    [EXITED] bar.example.com: Exit code 0. Took 5 seconds

    $ echo -e "foo.example.com\\nbar.example.com" | ./knife/execute.rb --command 'echo hello && sleep 5 && echo world' --output grouped

    [CONNECTION] foo.example.com
    [CONNECTION] bar.example.com
    [STDOUT] foo.example.com: hello
    [STDOUT] foo.example.com: world
    [EXITED] foo.example.com: Exit code 0. Took 5 seconds
    [STDOUT] bar.example.com: hello
    [STDOUT] bar.example.com: world
    [EXITED] bar.example.com: Exit code 0. Took 5 seconds

    $ echo -e "foo.example.com\\nbar.example.com" | ./knife/execute.rb --command 'echo hello && sleep 5 && echo world' --output none

  Machine Readable Output:
    This program prints command output and events to STDOUT. Ultimately the output is unstructured. You can capture a machine readable version of the output (only
    available at the end of this program's execution) by passing --json_output_file FILE_NAME.json.

Options
BANNER
  option :username,
         short: '-u USER',
         long: '--username USER',
         default: 'knife',
         description: 'SSH user. Set PASSWORD environment variable to pass user\s password'

  option :identity_file,
         short: '-i FILE',
         long: '--identity_file FILE',
         description: 'User identity file'

  option :command,
         short: '-c COMMAND',
         long: '--command COMMAND',
         required: true,
         description: 'command to run'

  option :concurrency,
         short: '-n NUM',
         long: '--concurrency NUM',
         default: 50,
         description: 'Number of nodes to run command on at once',
         proc: proc { |v| v.to_i }

  option :output,
         short: '-o VAL',
         long: '--output VAL',
         in: %w(realtime grouped none),
         default: 'realtime',
         description: 'When to print command output. One of none, realtime or grouped. See --help for more info'

  option :json_output_file,
         long: '--json_output_file FILE_NAME',
         description: 'file name to write all command out in json format'

  option :connection_output,
         long: '--[no-]connection_output',
         boolean: true,
         default: true,
         description: 'Print or supress connection to remote host output'

  option :command_output,
         long: '--[no-]command_output',
         boolean: true,
         default: true,
         description: 'Print or supress command output (stdout, stderr)'

  option :idle_output,
         long: '--[no-]idle_output',
         boolean: true,
         default: true,
         description: 'Print or supress idle output. Every 30 seconds of inactivity, print a list of hostname that have not yet completed'

  option :exit_output,
         long: '--[no-]exit_output',
         boolean: true,
         default: true,
         description: 'Print or supress remote connection exit output.'

  option :error_output,
         long: '--[no-]error_output',
         boolean: true,
         default: true,
         description: 'Print or supress errors when connecting to remote host.'

  attr_writer :output_io

  def no_output?
    config[:output] == 'none'
  end

  def realtime_output?
    return false if no_output?
    config[:output] == 'realtime'
  end

  def connection_output?
    return false if no_output?
    config[:connection_output]
  end

  def command_output?
    return false if no_output?
    config[:command_output]
  end

  def idle_output?
    return false if no_output?
    config[:idle_output]
  end

  def exit_output?
    return false if no_output?
    config[:exit_output]
  end

  def error_output?
    return false if no_output?
    config[:error_output]
  end

  def run(argv = ARGV)
    parse_options(argv)

    @multi_ssh   = Sbg::MultiSSH.new(concurrent_connections: config[:concurrency])
    @output_io ||= STDOUT
    @output_io.sync = true

    attach_handlers

    execute_commands

    write_json_output_file if config[:json_output_file]
  end

  private

  def execute_commands
    if ENV['NODE_LIST']
      @completed_hosts = @multi_ssh.execute(ENV['NODE_LIST'].split("\n"), config[:command], connection_options)
    else
      @completed_hosts = {}
      read_lines_from_pipe(STDIN) do |nodes|
        @completed_hosts.merge!(@multi_ssh.execute(nodes, config[:command], connection_options))
      end
    end
  end

  def write_json_output_file
    File.open(config[:json_output_file], 'w') do |file|
      nodes_hash = @completed_hosts.each_with_object({}) do |(hostname, data), ret|
        ret[hostname] = data.to_h
      end

      begin
        file.write(JSON.pretty_generate(nodes_hash))
      rescue => error
        @output_io.puts("Error generating json output of command results: #{error}")
      end
    end
  end

  def attach_handlers
    attach_connection_handler if connection_output?
    attach_error_handler if error_output?
    attach_idle_handler if idle_output?
    attach_realtime_command_handler if command_output? && realtime_output?
    attach_grouped_command_handler if command_output? && !realtime_output?
    attach_exit_handler if exit_output?
  end

  def connection_options
    {
      user: config[:username],
      non_interactive: true,
      paranoid: false
    }.tap do |opts|
      opts[:keys]     = config[:identity_file] if config[:identity_file]
      opts[:password] = ENV['PASSWORD'] if ENV['PASSWORD']
    end
  end

  def attach_connection_handler
    @multi_ssh.on_connect do |hostname|
      write_output('CONNECTION', hostname, :blue, "Running #{config[:command]}")
    end
  end

  def attach_realtime_command_handler
    { stdout: :cyan, stderr: :red }.each do |stream, color|
      @multi_ssh.send("on_#{stream}") do |hostname, data|
        data.split("\n").map do |line|
          write_output(stream.to_s.upcase, hostname, color, line)
        end
      end
    end
  end

  def attach_grouped_command_handler
    @multi_ssh.on_exit do |hostname, command_run|
      command_hash = command_run.to_h
      { stdout: :cyan, stderr: :red }.each do |stream, color|
        command_hash[stream].split("\n").each do |line|
          write_output(stream.to_s.upcase, hostname, color, line)
        end
      end
    end
  end

  def attach_exit_handler
    @multi_ssh.on_exit do |hostname, command_run|
      exit_code  = command_run.exit_code
      time_taken = command_run.formatted_time_taken
      color      = (exit_code == 0) ? :green : :red
      write_output('EXITED', hostname, color, "Exit code #{exit_code}. Took #{time_taken}.")
    end
  end

  def attach_error_handler
    @multi_ssh.on_error do |hostname, error|
      write_output('ERROR', hostname, :red, error)
    end
  end

  def attach_idle_handler
    @multi_ssh.on_idle do |hosts_left|
      case hosts_left.length
      when 0
        @output_io.puts('[MONITOR] 0 nodes left... This suggests a bug has occured if you can see this')
      when 1...5
        @output_io.puts("[MONITOR] Final #{hosts_left.length} node(s) remaining: #{hosts_left.join(', ')}")
      else
        @output_io.puts("[MONITOR] Still #{hosts_left.length} nodes to go")
      end
    end
  end

  def write_output(type, hostname, color, remaining)
    @output_io.puts("#{HighLine.color("[#{type}] #{hostname}", color)}: #{remaining}")
  end
end

cli = MultiSSHCli.new
cli.run
