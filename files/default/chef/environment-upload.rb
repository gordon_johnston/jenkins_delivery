#!/opt/chefdk/embedded/bin/ruby
# encoding: utf-8

require 'rubygems'
require 'colorize'
require 'open3'

# Need to set this for running from groovy
ENV['LANG'] = 'en_GB.UTF-8'
# Real-time please!
STDOUT.sync = true

require '/opt/jenkins/chef/logger.rb'
logger = Log.new

def fail_run(reason=nil, logger)

    unless reason.nil?
        logger.error reason
    end
    logger.error "Run failed!"
    exit 1
end

# expects a numeric return from fn
def timed_function(fn, desc, logger)

    logger.info "Starting #{desc}..."
    start  = Time.now

    ret = send(fn, logger)

    finish = Time.now - start
    mins   = finish / 60
    finish = finish % 60

    fin = "Finished #{desc} in #{mins.to_i} mins #{finish.round( 2 )} sec"

    if ret == 0
        logger.notice "#{fin}."
    else
        fail_run("#{fin} with #{ret} error#{ret == 1 ? '' : 's'}.", logger)
    end
end

def run_command(cmd, logger)
    stdout, stderr, status = Open3.capture3 cmd
    if status.success?
      logger.info "#{stdout}"
    else
      logger.error "Command failed: #{stderr} #{stdout}"
    end
    status.exitstatus
end

def environment_upload(logger)
  run_command "knife environment from file -c #{$KNIFE} envs/#{$ENV}.json", logger
end

# get our args
unless ARGV.length >= 1
    puts 'Usage: environment-upload.rb <environment>'.colorize( :red )
    exit 1
end
$ENV   = ARGV[0]

require 'json'

chef_api=JSON.parse(File.read("/opt/jenkins/orgs/#{$ENV}-org.json"))['chef-api']

#
# knife config
$KNIFE = "/opt/jenkins/orgs/#{chef_api}/#{$ENV}-knife.rb"

unless File.file?( $KNIFE )
    fail_run("Knife config #{$KNIFE} does not exist, please verify the slave has the correct knife config.", logger)
end
logger.info "Using Knife config #{$KNIFE}"

# change dir to our repo
timed_function('environment_upload', 'environment upload', logger)
