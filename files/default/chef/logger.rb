require 'colorize'

class Log
  def initialize()
  end

  def log_line(clr, lvl, str)
    puts "[#{Time.new}] #{lvl.ljust(6)} #{str.chomp}".colorize(clr)
  end

  def info(str)
    log_line(:blue, 'INFO', str)
  end

  def notice(str)
    log_line(:green, 'NOTICE', str)
  end

  def warn(str)
    log_line(:yellow, 'WARN', str)
  end

  def error(str)
    log_line(:red, 'ERROR', str)
  end

  def debug(str)
    if ENV['DEBUG']
      log_line(:magenta, 'DEBUG', str)
    end
  end
end
