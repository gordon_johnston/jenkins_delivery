/**
 * loop()
 * Convinence function to loop over a Map or List.
 *
 * Usage: loop(myMap) { key, value -> doSomething(key, value) }
 * Usage: loop(myList) { value -> doSomething(value) }
 *
 * Workflow plugin will want to serialize the state of a script at any time.
 * This ensures a script is durable to Jenkins restarts. Unfortunately many
 * objects in Java are not serializable.
 *
 * Some iterators are not serializable. Trying to iterate over a HashMap
 * will give the following errors;
 * * java.io.NotSerializableException: java.util.HashMap$KeySet
 * * java.io.NotSerializableException: java.util.HashMap$Node
 *
 * This function uses simple c style loops to work around this,
 * while keeping the calling code neater
 */
def call(Map subject, Closure body) {
  def keys = subject.keySet().toArray()

  for (def i = 0; i < keys.length; i++) {
    def key = keys[i]
    body.call(key, subject.get(key))
  }
}

def call(List subject, Closure body) {
  for(def i = 0; i < subject.size(); i++) {
    body.call(subject.get(i))
  }
}
