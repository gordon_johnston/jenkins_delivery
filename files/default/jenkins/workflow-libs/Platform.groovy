import groovy.json.JsonSlurper

String environmentToServer(String environment) {
  node('master') {
    String inputFile = new File("/opt/jenkins/orgs/${environment.toLowerCase()}-org.json").getText('UTF-8')
    def orgConf   = new JsonSlurper().parseText(inputFile)
    orgConf['chef-api']
  }
}

String environmentToRepo(String environment) {
  node('master') {
    String inputFile = new File("/opt/jenkins/orgs/${environment.toLowerCase()}-org.json").getText('UTF-8')
    def orgConf   = new JsonSlurper().parseText(inputFile)
    orgConf['integration-repo']
  }
}

String environmentToAdminUser(String environment) {
  node('master') {
    String inputFile = new File("/opt/jenkins/orgs/${environment.toLowerCase()}-org.json").getText('UTF-8')
    def orgConf   = new JsonSlurper().parseText(inputFile)
    orgConf['admin-user']
  }
}
