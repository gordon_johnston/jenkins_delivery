void setDisplayName(String name) {
  currentBuild.displayName = truncate(name, 50)
}

void setDescription(String description) {
  currentBuild.setDescription(description)
}

void appendDisplayName(String name) {
  setDisplayName("${currentBuild.displayName} ${name}")
}

void markAsSuccess() {
  currentBuild.result = 'SUCCESS'
}

void markAsUnstable() {
  currentBuild.result = 'UNSTABLE'
}

void markAsFailed() {
  currentBuild.result = 'FAILURE'
}

private String truncate(String value, def length) {
  def trunateToLength = length - 3
  if (value.length() < trunateToLength) {
    return value
  }

  value.substring(0, trunateToLength) + '...'
}
