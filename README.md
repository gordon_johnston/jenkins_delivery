Jenkins Delivery
================

## Pre-Requsites
----------------
### The ChefDK
The most recent version for your platform should work, this has been tested with ChefDK 0.14.25
* Mac - https://packages.chef.io/stable/mac_os_x/10.11/chefdk-0.14.25-1.dmg
* Red Hat Linux - https://packages.chef.io/stable/el/6/chefdk-0.14.25-1.el6.x86_64.rpm
* Ubuntu - https://packages.chef.io/stable/ubuntu/12.04/chefdk_0.14.25-1_amd64.deb

### IAM Role
You will need an IAM role configured in AWS that has permission to modify Route53 DNS records and permission to create EC2 instances

_Creating the IAM Role_

1. Select Identity and Access Management (IAM)
2. Select Roles
3. Create new role
4. Call it 'jenkins-delivery' 
5. Select a role type of 'Amazon EC2'
6. Attach policies 'AmazonRoute53FullAccess' and 'AmazonEC2FullAccess' 
7. Next, and Create Role

### AWS Virtual Private Cloud

You will need an AWS VPC to create the delivery system in.

_Creating the VPC_

1. Select the region in which you are deploying the system, and  Virtual Private Cloud (VPC)   
2. Start the VPC configuration wizard  and choose 'VPC with Single Public Subnet'   
3. Choose a VPC name.  Change the IP addressing if you want to. Make sure 'Enable DNS  hostnames' is enabled.   
4. Go to 'Subnets', select the public  subnet you just created, select 'Subnet Actions' and choose 'Modify  Auto-Assign Public IP', make sure it is enabled.

### EC2 Security Groups
Two security groups for the delivery system. One to allow inbound access from your administrative location. One more to allow access between each of the instances inside the VPC.

_Creating the Security Groups_

1. Select the region in which you are deploying the system, and Virtual Private Cloud (VPC)
2. Security Groups
3. Create, name tag "admin-access' and description 'Administrative access to Jenkins Delivery'. Make sure the group is created in the correct VPC. Create.
4. Select the new group, allow inbound access on ports 22 and 443 from your administrative address range.
5. Create, name tag 'delivery-system' and description 'Access internal to Jenkins Delivery'. Make sure the group is created in the correct VPC. Create.
6. Select the new group. Add a new inbound rule, allowing ALL TCP and set the source to the security group created in the previous step

### AWS Account Credentials
Your AWS access credentials should be in ~/.aws/config as standard Amazon shared credentials configuration e.g.
```
[default]
aws_access_key_id = XXXXXXX
aws_secret_access_key = XXXXXXX
```

Amazon have plenty of documentation on using shared credential profiles. See https://blogs.aws.amazon.com/security/post/Tx3D6U6WSFGOK2H/A-New-and-Standardized-Way-to-Manage-Credentials-in-the-AWS-SDKs

### Delegated Route53 Domain
A DNS zone that is delegated to Amazon's Route53. Ideally a subdomain. Follow this guide: http://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingNewSubdomain.html

## Creating the System
----------------------

### Fork jenkins_delivery
Fork the repository at https://bitbucket.org/harriga/jenkins_delivery and clone to your local system.
```
cd jenkins_delivery
```

### Create SSH Keys
We need to create some SSH keys. 

First, an EC2 SSH keypair for provisioning the nodes

_Creating the Keypair_

1. In the EC2 console, select the region in which you plan to deploy the system
2. Select Key Pairs
3. Create Key Pair, call it jenkins-delivery. The key will download to your local machine. Save it to:
```
jenkins_delivery/keys/aws
```
Second, an SSH key that can commit and tag your repositories. You will need to give access to this key to your jenkins_delivery repository and any other repositories that you will build

Github: https://developer.github.com/guides/managing-deploy-keys/
Bitbucket: https://confluence.atlassian.com/display/BITBUCKET/Use+deployment+keys
```
cd ~/jenkins_delivery
ssh-keygen -f keys/git
```
Lastly, an SSH key that will be used for orchestration inside the AWS system after provisioning
```
cd ~/jenkins_delivery
ssh-keygen -f keys/knife
```

### Populate Data Bags
This will convert the SSH keys created and stored in ./keys to JSON, and populate the Chef data bags stored in data_bags/keys
```
cd ~/jenkins_delivery
./tools/keys2json.rb
```

### Setup Test Kitchen
Configure ```.kitchen.local.yml``` so it is appropriate for your install. See ```.kitchen.local.example```.
```
# .kitchen.local.yml
# vi: set tabstop=2 :
# vi: set shiftwidth=2 :
---
driver:
  name: ec2
  aws_ssh_key_id: jenkins-delivery # The AWS ssh key ID to use for provisioning. Should be in your ssh-agent
  iam_profile_name: jenkins-delivery
  shared_credentials_profile: default
  security_group_ids: ["sg-xxxxxxxx", "sg-xxxxxxxx"] # Security groups to attach to all instances
  region: eu-central-1  # The AWS region
  subnet_id: subnet-xxxxxxxx # The subnet ID in which to build the chef, supermarket and jenkins instances
  instance_type: m4.large # Instance type for the above
  image_id: ami-c74789a9 # THe AMI to use to build the instances on. This is region specific. This one is Centos 7 in eu-central-1, and you will have to have already have to have accepted the EULA for running this AMI. Launch the AMI in the console to check you can before trying Kitchen.
platforms:
  - name: centos7
    transport:
      username: centos   # The AMI SSH username (centos for Centos official amis)
```

### Configure Delivery Environment
Edit the delivery environment configuration and customise it. The nameserver is in AWS is usually .2. You don't need to change it if you picked the default VPC subnet IP allocations.
```
vi envs/management.json
```

```
  "default_attributes": {
     "resolver": {
       "nameservers": ["10.0.0.2"]
     },
     "route53_domain": "your.route53.domain",
     "route53_zoneid": "XXXXXXXXXXXXXXX",
     "delivery_ssh_uri": "git@bitbucket.org:<user>/jenkins_delivery.git"
  },
```

### Setup ChefDK Paths
Run the following to setup your ChefDK environment (paths to gem, bundler, test kitchen, chef, etc)
```
eval $(chef shell-init sh)
```

### Install kitchen-sync
Install the kitchen-sync rubygem, for the Test Kitchen rsync transport
```
gem install kitchen-sync
```

### Setup SSH Agent
Setup your AWS SSH key and SSH agent locally
```
chmod 600 ./keys/aws
eval $(ssh-agent)
ssh-add ./keys/aws
```

### Create the system
This can take up to an hour while the instances are created and configured, typically takes around 30 minutes.
```
kitchen converge
```

### Retrieve the password for the administrative Jenkins user
```
kitchen login master
sudo cat /opt/jenkins/secrets/initialAdminPassword
```

### Login to Jenkins
Login to Jenkins at ```https://jenkins-delivery.<your route53 domain>``` using admin/the password from above. The InitialSetup pipeline should already be running, once complete the system will be usable for continuous delivery projects.
